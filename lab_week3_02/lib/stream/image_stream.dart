import 'dart:html';
import 'package:flutter/material.dart';

class ColorStream {
  // Stream colorStream;

  Stream<Uri> getImage() async* {
    int count = 0;
    final List<Uri> Image = [
      Uri.https('jsonplaceholder.typicode.com', 'photos/1'),

    ];
    count += 1;
    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return Image[index];
    });
  }
}