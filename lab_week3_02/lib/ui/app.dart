import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:image_viewer/stream/image_stream.dart';
import '../model/image_model.dart';
import 'package:http/http.dart' as http;




// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
  
}
/*
class AppState extends State<App> {
  int counter = 0;

    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Image Viewer - v0.0.8'),),
          body: Text('Display list of images here. counter = $counter'),
          floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: fetchImage
          ),
        )
    );

    return appWidget;
  }

}
*/


class HomePageState extends State<StatefulWidget> {
  Color bgTextColor = Colors.blueGrey;
  ColorStream ImageStream = ColorStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Image',

      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        backgroundColor: bgTextColor,
        body: Text('Welcome to Stream Image'),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            showImage();
          },
        ),
      ),
    );
  }

  showImage() async {
    ImageStream.getImage().listen((eventImage) {
      setState(() {
        print(eventImage);
        Uri url = eventImage;
        http.get(url).then(
                (result) {
              var jsonRaw = result.body;
              var jsonObject = json.decode(jsonRaw);
              // var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
              var imageModel = ImageModel.fromJson(jsonObject);

              print(imageModel);
            }
        );

      });
    });
  }
}